import XCTest
@testable import CustomFrameworkProject

final class CustomFrameworkProjectTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(CustomFrameworkProject().text, "Hello, World!")
    }
}
